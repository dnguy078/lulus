package shared

type Record struct {
	ID           string       `json:"id"`
	BillingInfo  BillingInfo  `json:"billing"`
	ShippingInfo ShippingInfo `json:"shipping"`
}

type BillingInfo struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

type ShippingInfo struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}
