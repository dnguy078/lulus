package cmd

import (
	"github.com/dnguy078/lulus/daemon"
	"github.com/spf13/cobra"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "server",
	RunE:  startServer,
}

func startServer(cmd *cobra.Command, args []string) error {
	s, err := daemon.New()
	if err != nil {
		return err
	}
	if err := s.Start(); err != nil {
		return err
	}
	return nil
}
