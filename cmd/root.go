package cmd

import (
	"github.com/spf13/cobra"
)

// RootCmd is a cobra command, attach other cobra commands to RootCmd
var RootCmd = &cobra.Command{}

func init() {
	RootCmd.AddCommand(serverCmd)
	RootCmd.AddCommand(billingServerCmd)
}
