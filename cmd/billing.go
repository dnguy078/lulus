package cmd

import (
	"context"
	"errors"
	"log"
	"net"

	"github.com/dnguy078/lulus/proto"

	"github.com/spf13/cobra"
	"golang.org/x/sync/syncmap"
	"google.golang.org/grpc"
)

var billingServerCmd = &cobra.Command{
	Use:   "billing",
	Short: "billing",
	RunE:  startBillingServer,
}

type billingGRPCServer struct {
	s *grpc.Server
	// thread safe map; also could have wrapped Go's default map around a RW mutex
	db syncmap.Map
}

func NewBillingGRPCServer() *billingGRPCServer {
	// TODO: use an GRPC intercepter for logging, will log in function for now
	bs := &billingGRPCServer{
		db: syncmap.Map{},
		s:  grpc.NewServer(),
	}

	proto.RegisterBillingServer(bs.s, bs)
	return bs
}

func (bs *billingGRPCServer) CreateEntry(ctx context.Context, req *proto.BillingRequest) (*proto.BillingResponse, error) {
	log.Printf("received request to create new billing entry, requestID: %v name: %v\n", req.RequestID, req.Name)
	data := &proto.BillingResponse{
		RequestID: req.RequestID,
		Name:      req.Name,
		Address:   req.Address,
		City:      req.City,
		State:     req.State,
	}
	bs.db.Store(req.RequestID, data)
	return data, nil
}

// used for testing purposes
func (bs *billingGRPCServer) getEntry(requestID string) (*proto.BillingResponse, error) {
	v, ok := bs.db.Load(requestID)
	if !ok {
		return &proto.BillingResponse{}, errors.New("billing record not found")
	}
	r, ok := v.(*proto.BillingResponse)
	if !ok {
		return &proto.BillingResponse{}, errors.New("incorrect type")
	}
	return r, nil
}

func (bs *billingGRPCServer) Stop() {
	bs.s.GracefulStop()
}

func startBillingServer(cmd *cobra.Command, args []string) error {
	bs := NewBillingGRPCServer()
	lis, err := net.Listen("tcp", ":3001")
	if err != nil {
		return err
	}
	log.Println("starting billing grpc service")
	return bs.s.Serve(lis)
}
