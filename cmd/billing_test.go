package cmd

import (
	"context"
	"testing"

	"github.com/dnguy078/lulus/proto"
)

func TestCreateEntry(t *testing.T) {
	b := NewBillingGRPCServer()
	var requestID = "fake-abcd-1234"

	if _, err := b.CreateEntry(context.TODO(), &proto.BillingRequest{
		RequestID: requestID,
		Name:      "fakename",
		Address:   "fakeaddress",
		City:      "fakecity",
		State:     "fakeCA",
	}); err != nil {
		t.Fatal("unexpected error writing to billing map")
	}

	data, err := b.getEntry(requestID)
	if err != nil {
		t.Errorf("expected to find record, err: %v", err)
	}
	if data.Name != "fakename" {
		t.Errorf("expected record's name to be %v, got: %v", "fakename", data.Name)
	}
	if data.Address != "fakeaddress" {
		t.Errorf("expected record's address to be %v, got: %v", "fakeaddress", data.Address)
	}
	if data.City != "fakecity" {
		t.Errorf("expected record's city to be %v, got: %v", "fakecity", data.City)
	}

	if data.State != "fakeCA" {
		t.Errorf("expected record's state to be %v, got: %v", "fakeCA", data.State)
	}
}
