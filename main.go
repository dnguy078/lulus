package main

import (
	"log"

	"github.com/dnguy078/lulus/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		log.Fatalf("unable to execute command: %s", err)
	}
}
