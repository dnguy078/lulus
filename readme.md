

## Running locally 
To Run:
  ```
  dep ensure
  docker-compose build && docker-compose up 
  ```
To Test: 
```
	make test
```
Run locally with dependent grpc services running:
```
	dep ensure -v
	go run main.go billing&
	go run main.go server
```


## API
	// curl -X POST localhost:3000/record -d
	```
	{
		"id": "something",
		"billing": {
			"name": "name",
			"address": "address",
		}
		"shipping": {
			"name": "name",
			"address": "address",
		}
	}
	```

## Updating Protobufs: 
```
go get -u github.com/golang/protobuf/protoc-gen-go
protoc --go_out=plugins=grpc:. proto/*.proto
```
