package endpoints

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/dnguy078/lulus/shared"

	"golang.org/x/time/rate"
)

type RecordHandler struct {
	Dispatcher Broker
}

type Broker interface {
	Send(shared.Record) error
}

type RecordRequest struct {
	Name        string  `json:"name"`
	ProduceCode string  `json:"produce_code"`
	Price       float64 `json:"unit_price"`
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func newLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

// WithLogging is a middleware that wraps a http handler and logs the statuscodes.
func WithLogging(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		lrw := newLoggingResponseWriter(w)
		next.ServeHTTP(lrw, r)
		statusCode := lrw.statusCode
		log.Printf("Received Request %s %s - %d %s", r.Method, r.URL.Path, statusCode, http.StatusText(statusCode))
	}
}

// WithRateLimit is a middleware that limits the number of request for a HTTP handler.
// A Limiter controls how frequently events are allowed to happen.
// It implements a "token bucket" of size b, initially full and refilled
// at rate r tokens per second.
func WithRateLimit(next http.HandlerFunc, limiter *rate.Limiter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if limiter.Allow() == false {
			log.Printf("Received Request %s %s - %d %s", r.Method, r.URL.Path, http.StatusTooManyRequests, http.StatusText(429))
			http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(w, r)
	}
}

func (rh *RecordHandler) Record(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "method is not supported", http.StatusBadRequest)
		return
	}

	record := &shared.Record{}
	if err := json.NewDecoder(r.Body).Decode(record); err != nil {
		http.Error(w, "error decoding request body", http.StatusBadRequest)
		return
	}
	if record.ID == "" {
		http.Error(w, "requestID cannot not be empty", http.StatusBadRequest)
		return
	}

	if err := rh.Dispatcher.Send(*record); err != nil {
		http.Error(w, "unable to process message at this time", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}
