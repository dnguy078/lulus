package endpoints

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"sync"
	"testing"

	"github.com/dnguy078/lulus/shared"
	"golang.org/x/time/rate"
)

type fakeHandler struct {
	mu     sync.Mutex
	called int
}

func (fh *fakeHandler) Handler(w http.ResponseWriter, r *http.Request) {
	fh.mu.Lock()
	defer fh.mu.Unlock()
	fh.called++
}

func TestWithRateLimit(t *testing.T) {
	fh := &fakeHandler{}
	limiter := rate.NewLimiter(100, 1)
	rl := WithRateLimit(fh.Handler, limiter)

	ts := httptest.NewServer(rl)
	fmt.Println(ts.URL)
	defer ts.Close()

	testCases := []struct {
		rated          bool
		expectedStatus int
	}{
		{
			expectedStatus: 200,
		},
		{
			rated:          true,
			expectedStatus: 429,
		},
	}

	for _, c := range testCases {
		if c.rated {
			for i := 0; i < 100; i++ {
				_, err := http.Get(ts.URL)
				if err != nil {
					t.Error(err)
				}
			}
		}

		resp, err := http.Get(ts.URL)
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != c.expectedStatus {
			t.Error(resp.StatusCode, c.expectedStatus)
		}
	}
}

type fakeBroker struct {
	err error
}

func (f *fakeBroker) Send(shared.Record) error {
	return f.err
}

func TestRecord(t *testing.T) {
	cases := []struct {
		name    string
		payload string
		err     error

		expectedStatusCode int
		expectedBody       string
	}{
		{
			name:               "success",
			payload:            `{"id":"test product","billing": {"name": "somename", "address": "some billing adressw"} }`,
			expectedStatusCode: http.StatusAccepted,
		},
		{
			name:               "bad request unmarshalling",
			payload:            `{"id":"test product","billing": {"name": "somename", "address": "some billing adressw"}`,
			expectedStatusCode: http.StatusBadRequest,
			expectedBody:       "error decoding request body\n",
		},
		{
			name:               "error",
			payload:            `{"id":"test product","billing": {"name": "somename", "address": "some billing adressw"} }`,
			err:                errors.New("some error"),
			expectedStatusCode: http.StatusInternalServerError,
			expectedBody:       "unable to process message at this time\n",
		},
	}

	for _, c := range cases {
		fb := &fakeBroker{
			err: c.err,
		}
		rh := &RecordHandler{
			Dispatcher: fb,
		}

		r, err := http.NewRequest("POST", "/record", strings.NewReader(c.payload))
		if err != nil {
			t.Fatal(err)
		}
		w := httptest.NewRecorder()

		rh.Record(w, r)
		if c.expectedStatusCode != w.Code {
			t.Errorf("test - %s, expected %d, got %d", c.name, c.expectedStatusCode, w.Code)
		}
		if c.expectedBody != w.Body.String() {
			t.Errorf("test - %s, expected %q, got %q", c.name, c.expectedBody, w.Body.String())
		}
	}
}
