package services

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/dnguy078/lulus/proto"
	"github.com/dnguy078/lulus/shared"
	"google.golang.org/grpc"
)

const (
	maxWorkers = 100
)

// Dispatcher allows workers to consume off a channel of records
// Handles graceful shutdown of workers
type Dispatcher struct {
	messages      chan shared.Record
	billingClient proto.BillingClient
	quit          chan bool
}

type DispatcherConfig struct {
	BillingGRPCAddr   string
	TransportGRPCAddr string
	ServerTimeout     time.Duration
}

func NewDispatcher(cfg DispatcherConfig) (*Dispatcher, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cfg.ServerTimeout)
	defer cancel()
	conn, err := grpc.DialContext(ctx, cfg.BillingGRPCAddr, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("unable to establish a grpc connection, err: %v", err)
	}
	bc := proto.NewBillingClient(conn)

	return &Dispatcher{
		messages:      make(chan shared.Record, 100),
		quit:          make(chan bool),
		billingClient: bc,
	}, nil
}

func (d *Dispatcher) Send(r shared.Record) error {
	if len(d.messages) >= 100 {
		return errors.New("unable to process message at this time")
	}
	d.messages <- r
	return nil
}

func (d *Dispatcher) Stop() {
	close(d.quit)
}

func (d *Dispatcher) Run() {
	for i := 0; i < maxWorkers; i++ {
		worker := Worker{
			id:            i,
			billingClient: d.billingClient,
			jobQueue:      d.messages,
			quit:          d.quit,
		}

		go worker.Start()
	}
}
