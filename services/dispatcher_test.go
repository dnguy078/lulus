package services

import (
	"context"
	"errors"
	"net"
	"sync"
	"testing"
	"time"

	"github.com/dnguy078/lulus/proto"
	"github.com/dnguy078/lulus/shared"
	"google.golang.org/grpc"
)

type fakeBillingServer struct {
	err    error
	mu     sync.Mutex
	called int
}

func (fb *fakeBillingServer) CreateEntry(ctx context.Context, in *proto.BillingRequest) (*proto.BillingResponse, error) {
	fb.mu.Lock()
	fb.called++
	fb.mu.Unlock()

	return &proto.BillingResponse{}, fb.err
}

func setup() (*grpc.Server, *fakeBillingServer) {
	s := grpc.NewServer()
	fbs := &fakeBillingServer{
		mu: sync.Mutex{},
	}
	proto.RegisterBillingServer(s, fbs)
	return s, fbs
}

func TestDispatcher(t *testing.T) {
	testCases := []struct {
		description string
		port        string
		expectedErr error
	}{
		{
			description: "unable to connect",
			port:        ":1234",
			expectedErr: errors.New("unable to establish a grpc connection, err: context deadline exceeded"),
		},
		{
			description: "connect",
			port:        ":3001",
		},
	}

	for _, c := range testCases {
		s, _ := setup()
		lis, err := net.Listen("tcp", c.port)
		if err != nil {
			t.Fatal(err)
		}
		go s.Serve(lis)
		defer s.Stop()

		cfg := DispatcherConfig{
			BillingGRPCAddr: "localhost:3001",
			ServerTimeout:   1 * time.Second,
		}
		_, err = NewDispatcher(cfg)
		if err != nil {
			if err.Error() != c.expectedErr.Error() {
				t.Errorf("Test %s - expected %q got %q", c.description, c.expectedErr, err)
			}
		}
	}
}

func TestSend(t *testing.T) {
	testCases := []struct {
		description string
		maxSend     int
		err         error
	}{
		{
			description: "Successful Send",
			maxSend:     1,
		},
		{
			description: "Max send",
			maxSend:     101,
			err:         errors.New("unable to process message at this time"),
		},
	}
	for _, c := range testCases {
		d := Dispatcher{
			messages: make(chan shared.Record, 100),
		}
		for i := 0; i <= c.maxSend; i++ {
			err := d.Send(shared.Record{ID: "somerecord"})
			if err != nil {
				if err.Error() != c.err.Error() {
					t.Errorf("Test %s - expected %q got %q", c.description, c.err, err)
				}
			}
		}
	}
}
