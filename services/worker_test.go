package services

import (
	"fmt"
	"net"
	"testing"
	"time"

	"github.com/dnguy078/lulus/shared"
)

func TestWorkerRun(t *testing.T) {
	s, fbs := setup()
	lis, err := net.Listen("tcp", ":3003")
	if err != nil {
		t.Fatal(err)
	}
	go s.Serve(lis)
	defer s.Stop()

	cfg := DispatcherConfig{
		BillingGRPCAddr: "localhost:3003",
		ServerTimeout:   1 * time.Second,
	}
	d, err := NewDispatcher(cfg)
	if err != nil {
		t.Fatal(err)
	}
	d.Run()
	for i := 0; i < 10; i++ {
		if err := d.Send(shared.Record{ID: fmt.Sprintf("somerecord-%d", i)}); err != nil {
			t.Fatal(err)
		}
	}
	time.Sleep(1 * time.Second)

	if fbs.called != 10 {
		t.Errorf("expected to be 10, got %d", fbs.called)
	}
}
