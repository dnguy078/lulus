package services

import (
	"context"
	"log"

	"github.com/dnguy078/lulus/proto"
	"github.com/dnguy078/lulus/shared"
)


type Worker struct {
	id            int
	jobQueue      chan shared.Record
	billingClient proto.BillingClient
	quit          chan bool
}

// Start method starts the run loop listening for a job to come in and also listening for a quit signal to stop
func (w Worker) Start() {
	for {
		select {
		case job, ok := <-w.jobQueue:
			if !ok {
				return
			}
			w.Run(job)
		case <-w.quit:
			return
		}
	}
}

// Run processes a record and calls out to external GRPC services to update records
func (w Worker) Run(delivery shared.Record) {
	log.Printf("received delivery %s by worker %d\n", delivery.ID, w.id)
	ctx := context.Background()
	if _, err := w.billingClient.CreateEntry(ctx, &proto.BillingRequest{
		RequestID: delivery.ID,
		Name:      delivery.BillingInfo.Name,
		Address:   delivery.BillingInfo.Address,
	}); err != nil {
		log.Printf("error creating entry, requeuing id %s, err: %v\n", delivery.ID, err)
		w.Requeue(delivery)
	}
}

// Requeue retries a message onto the job queue
func (w Worker) Requeue(delivery shared.Record) {
	w.jobQueue <- delivery
}
