package daemon

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/dnguy078/lulus/endpoints"
	"github.com/dnguy078/lulus/services"

	"golang.org/x/time/rate"
)

type Server struct {
	router *http.ServeMux
	logger *log.Logger

	host string
	port string
}

func New() (*Server, error) {
	logger := log.New(os.Stdout, "http: ", log.LstdFlags)

	router := http.NewServeMux()
	cfg := services.DispatcherConfig{
		BillingGRPCAddr:   "billing:3001",
		TransportGRPCAddr: "transport:3002",
		ServerTimeout:     1 * time.Second,
	}
	dispatcher, err := services.NewDispatcher(cfg)
	if err != nil {
		return nil, err
	}
	dispatcher.Run()

	var limiter = rate.NewLimiter(100, 1)
	rh := &endpoints.RecordHandler{Dispatcher: dispatcher}
	router.HandleFunc("/record", endpoints.WithRateLimit(endpoints.WithLogging(rh.Record), limiter))
	return &Server{
		router: router,
		logger: logger,
	}, nil
}

func (s *Server) Start() error {
	s.logger.Println("starting server")
	return http.ListenAndServe(":3000", s.router)
}
