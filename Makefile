GOCMD=go
GOBUILD=$(GOCMD) build
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get -u

all: test
build:
	docker-compose build
test:
	$(GOTEST) ./... 
start:
	docker-compose up
deps:
	dep ensure -v
